package sandbox.controller

import org.springframework.cloud.gateway.mvc.ProxyExchange
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate

@RestController
@RequestMapping("/rest")
class RestClientController(private val userServiceApi: RestTemplate) {

    @GetMapping
    fun get(proxy: ProxyExchange<Any>): ResponseEntity<String> {
        val response = userServiceApi.getForObject("/userlink/get/1/1", String::class.java)
        return ResponseEntity.ok(response!!)
    }
}