package sandbox.controller

import mu.KotlinLogging
import org.springframework.cloud.gateway.mvc.ProxyExchange
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder

@RestController
@RequestMapping("/qiita")
class QiitaRestController(private val qiitaApi: RestTemplate) {

    private val logger = KotlinLogging.logger {}

    @GetMapping
    fun get(proxy: ProxyExchange<Any>): ResponseEntity<Any> {

        // call qiita user
        val getUserUrl = getQiitaUsersUrl()
        val userRequestMap = mapOf("userId" to "sora083")
        val userResponse: UserResponse? = qiitaApi.getForObject(getUserUrl, UserResponse::class.java, userRequestMap)
        logger.info("userResponse: $userResponse")

        // call qiita user's followees
        val getFolloweesUrl = getUserFolloweesUrl()
        val followeeRequestMap = mapOf("userId" to userResponse?.id)
        val followees: List<Followee>? = qiitaApi.getForObject(getFolloweesUrl, Array<Followee>::class.java, followeeRequestMap)?.toList()
        logger.info("followees: $followees")

        return ResponseEntity.ok(object {
            val user = userResponse
            val followees = followees
        })
    }

    fun getQiitaUsersUrl(): String {
        return UriComponentsBuilder
            .fromPath("/users/{userId}")
            .queryParam("page", 1)
            .queryParam("per_page", 5)
            .build()
            .toString()
    }

    fun getUserFolloweesUrl(): String {
        return UriComponentsBuilder
            .fromPath("/users/{userId}/followees")
            .queryParam("page", 1)
            .queryParam("per_page", 5)
            .build()
            .toString()
    }


    data class UserResponse(
            var description: String,
            var facebook_id: String,
            var followees_count: Int,
            var followers_count: Int,
            var github_login_name: String? = null,
            var id: String,
            var items_count: Int,
            var linkedin_id: String,
            var location: String? = null,
            var name: String,
            var organization: String,
            var permanent_id: Int,
            var profile_image_url: String,
            var team_only: Boolean,
            var twitter_screen_name: String,
            var website_url: String
    )

    data class Followee(
            val description: String,
            val facebook_id: String,
            val followees_count: Int,
            val followers_count: Int,
            val github_login_name: String,
            val id: String,
            val items_count: Int,
            val linkedin_id: String,
            val location: String,
            val name: String,
            val organization: String,
            val permanent_id: Int,
            val profile_image_url: String,
            val team_only: Boolean,
            val twitter_screen_name: String? = null,
            val website_url: String
    )
}