package sandbox.config.qiita

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.NotNull

@Component
@ConfigurationProperties(prefix = "qiita")
@Validated
class QiitaApiConfig {
    @NotNull
    lateinit var url: String
    var readTimeOut: Int? = 0
    var connectionTimeOut: Int? = 0
}
