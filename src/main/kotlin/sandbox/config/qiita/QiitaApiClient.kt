package sandbox.config.qiita

import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.ClientHttpRequestFactory
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.web.client.RestTemplate

@Configuration
class QiitaApiClient(private val qiitaApiConfig: QiitaApiConfig) {

    @Bean
    fun qiitaApi(): RestTemplate {
        //val requestFactory = clientHttpRequestFactory(qiitaApiConfig.readTimeOut!!, qiitaApiConfig.connectionTimeOut!!)
        return RestTemplateBuilder().rootUri(qiitaApiConfig.url).build()
        //return RestTemplate(requestFactory)
    }

    private fun clientHttpRequestFactory(readTimeout: Int, connectionTimeOut: Int): ClientHttpRequestFactory {
        val factory = HttpComponentsClientHttpRequestFactory()
        factory.setReadTimeout(readTimeout) // setting timeout as read timeout
        factory.setConnectTimeout(connectionTimeOut) // setting timeout as connect timeout
        return factory
    }
}