package sandbox.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.NotNull

@Component
@ConfigurationProperties(prefix = "cp")
@Validated
class CpUrlConfig {
    //@NotNull
    lateinit var userservice: String
}
